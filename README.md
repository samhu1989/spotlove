# SpotLove

A costum build of SpotMicro by Chinese lovers



采购清单：

| 物品                               | 淘宝链接                                                     | 价格                                | 备注                                                         |
| :--------------------------------- | ------------------------------------------------------------ | ----------------------------------- | ------------------------------------------------------------ |
| MG996R舵机                         | [安芯创达科技](https://item.taobao.com/item.htm?id=622090724759&ali_refid=a3_430673_1006:1271980114:N:w6Zrv4kGq4rYUeI5tfeVLA%3D%3D:22c5a83fffdc095dc07c7f37a81cc80f&ali_trackid=1_22c5a83fffdc095dc07c7f37a81cc80f&spm=a2e15.8261149.07626516002.29) | ￥13.8x12 = ￥165.6                 | 180度 12 个 （根据博友[麦克斯韦的妖精](https://blog.csdn.net/john_bian/article/details/52825214?locationNum=3&fps=1)堵转电流约1.5A）后续硬件升级应从这个舵机开始 |
| PCA9685 16路PWM 电机驱动模块       | [宙科电子](https://item.taobao.com/item.htm?id=613946876572&ali_trackid=2:mm_26632360_8858797_6542250051:1597668831_194_1752471522&spm=a2e15.8261149.07626516002.26&pvid=200_11.11.42.250_407_1597668401733&bxsign=tbk15976688312704e136c5a3e00f00e067d6cddee2248f7) | ￥58 x 2 = ￥116                    | 2个                                                          |
| 树莓派PI0W+自带SD卡                | [鼎开源硬件商城](https://item.taobao.com/item.htm?id=524721396904&ali_refid=a3_430673_1006:1110571867:N:Fi41GmiZpI7tB64m1DIahw%3D%3D:7636c9ad509908cb4ee1f5f3d8b4c992&ali_trackid=1_7636c9ad509908cb4ee1f5f3d8b4c992&spm=a2e15.8261149.07626516002.13) | ￥211                               | 选购03豪华基础配件套餐                                       |
| 5V直流降压稳压模块（逻辑电路供电） | [佳信微旗舰店](https://detail.tmall.com/item.htm?id=601488488830&ali_refid=a3_430673_1006:1231260087:N:U54ISFC0NjLzI2Utiksurw==:9804c0568c5b81ab693aa9bf0642eeae&ali_trackid=1_9804c0568c5b81ab693aa9bf0642eeae&spm=a2e15.8261149.07626516002.3&skuId=4382642763567) | ￥2.9                               |                                                              |
| 电源管理模块                       | [上海模友之家](https://item.taobao.com/item.htm?id=603259711113&ali_trackid=2:mm_26632360_8858797_6542250051:1597670027_157_854487085&spm=a2e15.8261149.07626516002.9&pvid=200_11.1.203.21_21227985_1597669912077&bxsign=tbk159767002707567fd939e3feb38a58be1757abe29a8ee) | ￥88.0                              |                                                              |
| 电池组                             | [耐杰旗舰店](https://detail.tmall.com/item.htm?id=520111996213&ali_trackid=2:mm_131341970_42698365_258142393:1597671770_268_1283320531&spm=a231o.7712113/g.1004.226&pvid=200_11.27.109.220_424012_1597671421793&bxsign=tbk1597671770962a1723fe6a9b6eff8149cba5b52874929&skuId=3404209065680) | ￥53 x (2~4)                        | 需要购买2800mah的3组或4组才能保证电流供应，但是不知道电池组的具体体积，最好先买一组回来试试看和狗身匹不匹配 |
| 液晶屏幕                           | [DFRobot官方品牌店](https://item.taobao.com/item.htm?spm=a230r.1.14.48.5a70188bNojIkV&id=568800610184&ns=1&abbucket=19#detail) | ￥45                                |                                                              |
| 万用表、电烙铁等工具套装           | [卧虎龙强强店](https://item.taobao.com/item.htm?spm=a230r.1.14.17.32345a3bVKHkXI&id=620737946301&ns=1&abbucket=19#detail) | ￥91.05                             | 20件套装                                                     |
| 导线                               | [兴垲线缆工厂店](https://shop60002064.taobao.com/)           | ￥13 + 8 +   1.17x5 + 1.5 = ￥26.85 | 额定10A 5米 额定2A10米 2芯控制线5米 电工胶布                 |
| 3D打印机                           | [极光尔沃华浔专卖店](https://detail.tmall.com/item.htm?spm=a220o.1000855.1998025129.1.48bc1d58HczZS7&pvid=06703502-5996-4dcf-9239-07935fda31b2&pos=1&acm=03054.1003.1.2768562&id=595459759530&scm=1007.16862.95220.23864_0_0&utparam=%7B%22x_hestia_source%22:%2223864%22,%22x_object_type%22:%22item%22,%22x_hestia_subsource%22:%22default%22,%22x_mt%22:0,%22x_src%22:%2223864%22,%22x_pos%22:1,%22wh_pid%22:-1,%22x_pvid%22:%2206703502-5996-4dcf-9239-07935fda31b2%22,%22scm%22:%221007.12144.95220.23864_0_0%22,%22x_object_id%22:595459759530%7D&sku_properties=5919063:6536025) | ￥1580+260 = 1840                   | 官方要求 打孔口0.6 层高 0.3 支撑材料高度差2mm，该机型口径0.4 层高可选 黑黄耗材130元一公斤 腿部将来可能需要tpu这样的柔性耗材 |
| USB示波器（可选）                  | [乐拓](https://item.taobao.com/item.htm?id=591985370762&ali_refid=a3_430673_1006:1106287607:N:SuEHPISrKPE5Il%2BlTOxROqDg4pel55r0:96dc45a9dc798ee623040077ade018b0&ali_trackid=1_96dc45a9dc798ee623040077ade018b0&spm=a2e15.8261149.07626516002.2) | ￥678                               | 买全功能五合一就够了，毕竟USB示波器采样率不太够 （20Mhz）好在便宜 |
| 总计                               |                                                              | ￥3477.9                            | 螺丝之类的配件没有计算在内，暂时也不知道要用啥螺丝，可在[易丝旗舰店](https://detail.tmall.com/item.htm?spm=a220m.1000858.1000725.46.3a4530fcfgWC6U&id=563599063055&skuId=4221594708391&areaId=430100&user_id=907568637&cat_id=2&is_b=1&rn=14fb221d67001f71aab2226e3b2f4bac)购买最多不过几十元，3D打印可能还需要砂纸和抛光液做表面处理 |



  





